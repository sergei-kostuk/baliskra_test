<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars_photo".
 *
 * @property int $id
 * @property int $car_id
 * @property int $photo_id
 * @property int $so
 * @property int $deleted
 * @property int $saved
 * @property string $created
 *
 * @property Cars $car
 */
class CarsPhoto extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'photo_id'], 'required'],
            [['car_id', 'photo_id', 'so', 'deleted', 'saved'], 'integer'],
            [['created'], 'safe'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'car_id' => Yii::t('user', 'Car ID'),
            'photo_id' => Yii::t('user', 'Photo ID'),
            'so' => Yii::t('user', 'So'),
            'deleted' => Yii::t('user', 'Deleted'),
            'saved' => Yii::t('user', 'Saved'),
            'created' => Yii::t('user', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    public function savePhoto(){
        $url = 'https://bilasolur.is/CarImage.aspx?s=19&c=' . $this->car->api_id . '&p=' . $this->photo_id . '&w=2500';
        $path = "./web/img/cars/" . $this->car_id;
//        echo $path . "/" . $this->id . ".jpg";
//        unlink($path. "/" . $this->id . ".jpg");
        if(file_exists($path. "/" . $this->id . ".jpg")){
            echo ' Photo Exist ';
            $r = $this->isDel();

            if($r == 2){
                unlink($path. "/" . $this->id . ".jpg");
                echo '=====DEL-----';
            }
            $this->saved = $r==2?$r:1;
//            var_dump($this->saved);
//            if($this->saved == 1){
//                $car = Cars::findOne($this->car_id);
//                $car->main_img = $this->id;
//                $car->save();
//            }
            $this->save();
            return false;
        }
        @mkdir($path, 0777, true);
        $ctx = stream_context_create(array('http'=>
            array(
                'timeout' => 20,  //1200 Seconds is 20 Minutes
            )
        ));
        $raw = file_get_contents($url,false,$ctx);

        if(file_put_contents($path . "/" . $this->id . ".jpg", $raw)){
//            echo "Photo Save";
            $r = $this->isDel();
            if($r == 2){
                unlink($path. "/" . $this->id . ".jpg");
                echo "\n\r==DEL==\n\r";
            }else{
                echo $r.'%';
            }
            $this->saved = $r==2?$r:1;
//            if($this->saved == 1){
//                $car = Cars::findOne($this->car_id);
//                $car->main_img = $this->id;
//                $car->save();
//            }
            $this->save();
        }else{
            die;
        }
        return true;
    }

    public function isDel(){
        // create images
        $i1 = @imagecreatefromstring(file_get_contents('./web/del.jpg'));
        $path = "./web/img/cars/" . $this->car_id. "/" . $this->id . ".jpg";

        list($width, $height) = getimagesize($path);
        echo '- '.$path ." - ";
        $i2 = @imagecreatefromstring(file_get_contents($path));
//        $i2_src = @imagecreatefromstring(file_get_contents($path));
//        $i2 = imagecreatetruecolor(320, 256);

//        imagecopyresized($i2,$i2_src,0,0,0,0,320,256,$width, $height);
//
//        imagejpeg($i2,'sm.jpg',100);
//        die;


// check if we were given garbage
//        if (!$i1) {
//            echo $argv[1] . ' is not a valid image';
//            exit(1);
//        }
//        if (!$i2) {
//            echo $argv[2] . ' is not a valid image';
//            exit(1);
//        }

// dimensions of the first image
        $sx1 = imagesx($i1);
        $sy1 = imagesy($i1);

// compare dimensions
        if ($sx1 !== imagesx($i2) || $sy1 !== imagesy($i2)) {
            echo "The images are not even the same size";
            exit(1);
        }

// create a diff image
        $diffi = imagecreatetruecolor($sx1, $sy1);
        $green = imagecolorallocate($diffi, 0, 255, 0);
        imagefill($diffi, 0, 0, imagecolorallocate($diffi, 0, 0, 0));

// increment this counter when encountering a pixel diff
        $different_pixels = 0;

// loop x and y
        for ($x = 0; $x < $sx1; $x++) {
            for ($y = 0; $y < $sy1; $y++) {

                $rgb1 = imagecolorat($i1, $x, $y);
                $pix1 = imagecolorsforindex($i1, $rgb1);

                $rgb2 = imagecolorat($i2, $x, $y);
                $pix2 = imagecolorsforindex($i2, $rgb2);

                if ($pix1 !== $pix2) { // different pixel
                    // increment and paint in the diff image
                    $different_pixels++;
                    imagesetpixel($diffi, $x, $y, $green);
                }

            }
        }


        if (!$different_pixels) {
            return 2;
        } else {
//            if (empty($argv[3])) {
//                $argv[3] = 'diffy.png'; // default result filename
//            }
//            imagepng($diffi, $argv[3]);
            $total = $sx1 * $sy1;

            return  number_format(100 * $different_pixels / $total, 2);
//            echo "$different_pixels/$total different pixels, or ", number_format(100 * $different_pixels / $total, 2), '%';
//            exit(1);
        }
    }


    public function afterSave($insert, $changedAttributes)
    {
//        echo '!!!';
//        var_dump($this->saved);
        if($this->saved == 1 && $this->so == 0){
            $car = Cars::findOne($this->car_id);
            $car->main_img = $this->id;
            $car->save();
        }
        return parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
