<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aukahlutir".
 *
 * @property int $id
 * @property string $name
 * @property int $cat
 * @property int $order
 *
 * @property AukahlutirCat $cat0
 */
class Aukahlutir extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aukahlutir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'cat', 'order'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['id'], 'unique'],
            [['cat'], 'exist', 'skipOnError' => true, 'targetClass' => AukahlutirCat::className(), 'targetAttribute' => ['cat' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cat' => 'Cat',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat0()
    {
        return $this->hasOne(AukahlutirCat::className(), ['id' => 'cat']);
    }
}
