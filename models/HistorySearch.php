<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history_search".
 *
 * @property int $id
 * @property int $user_id
 * @property string $params
 * @property int $email_notofication
 * @property int $push_notification
 * @property int $total
 * @property int $type_id
 * @property string $created
 * @property string $updated
 *
 * @property Subscriber $user
 */
class HistorySearch extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history_search';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'params'], 'required'],
            [['user_id', 'email_notofication', 'push_notification', 'total', 'type_id'], 'integer'],
            [['params'], 'string'],
            [['created', 'updated'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscriber::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'params' => Yii::t('app', 'Params'),
            'email_notofication' => Yii::t('app', 'Email Notofication'),
            'push_notification' => Yii::t('app', 'Push Notification'),
            'total' => Yii::t('app', 'Total'),
            'type_id' => Yii::t('app', 'Type ID'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Subscriber::className(), ['id' => 'user_id']);
    }

    public function cleanParams($params)
    {
        if(is_string($params)){
            $params = json_decode($params);
        }
        $json = file_get_contents(\yii\helpers\Url::to('@app/filter.json'));
        $json = json_decode($json);
        $res = [];
        foreach ($params as $k => $item) {
            if ($k == 'argerd') {
                if($item[0] != $json->argerd->mi || $item[1] != $json->argerd->ma){
                    $res[$k] = $item;
                }
                continue;
            }

            if($k == 'verd'){

                if($item[0] != $json->verd->mi*1000 || $item[1] != $json->verd->ma*1000){
                    $res[$k] = $item;
                }
                continue;
            }

            if($k == 'ekinn'){
                if($item[0] != $json->ekinn->mi || $item[1] != $json->ekinn->ma){
                    $res[$k] = $item;
                }
                continue;
            }

            if (!empty($item)) {
                $res[$k] = $item;
            }

        }
        return json_encode($res);
    }
}
