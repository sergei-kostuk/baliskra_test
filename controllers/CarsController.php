<?php

namespace app\controllers;

use app\models\Cars;
use app\models\CarsSearch;
use app\models\CarsSphinx;
use app\models\NewCarsBrands;
use app\models\NewCarsPackage;
use app\models\NewCarsPackageLib;
use app\widgets\Alert;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Yii;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\models\Alerts;
use yii\helpers\ArrayHelper;

class CarsController extends BaseAdmin
{

    /**
     * {@inheritdoc}
     */
/*
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'add', 'del', 'view', 'alerts'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'alerts'],
                        'allow' => true,
                        'roles' => ['carsView'],
                    ], [
                        'actions' => ['del', 'add'],
                        'allow' => true,
                        'roles' => ['carsAdd'],
                    ]
                ],
            ],
        ];
    }
*/
    public function actionIndex()
    {
        $searchModel = new CarsSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alerts::find()->where(['is', 'deleted', null])->orWhere(['deleted' => 0])->andWhere(['car_id' => $id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $car = Cars::findOne($id);
        return $this->render('view', ['car' => $car, 'dataProvider' => $dataProvider]);
    }

    public function actionAlerts()
    {
        $alerts = Alerts::find()->groupBy('car_id')->all();


        $dataProvider = new ActiveDataProvider([
            'query' => Cars::find()->where(['id' => ArrayHelper::getColumn($alerts, 'car_id')]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('alerts', ['dataProvider' => $dataProvider]);
    }

    public function actionAdd($id = 0)
    {
        $cars = new Cars();
        return $this->render('add', ['cars' => $cars]);
    }

    public function actionDel($id)
    {
        $car = Cars::findOne($id);

        if ($car) {
            $car->deleted = 1;
            $car->deleted_at = time();
            $car->save();
        }
        return $this->redirect("/cars");
    }


    public function actionTest($id = 0)
    {
        echo '<pre>';

        die;
    }

    public function actionUpdateParser($id){
        echo $id;
        echo '<pre>';
        echo exec("cd ../parse && node update_cars.js ".$id);
//        print_r(scandir("../parse"));
    }

}
