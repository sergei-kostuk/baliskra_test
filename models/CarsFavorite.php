<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars_favorite".
 *
 * @property int $id
 * @property int $car_id
 * @property int $sub_id
 * @property int $type_id
 * @property string $created
 *
 * @property AlertsType $type
 * @property Cars $car
 * @property Subscriber $sub
 */
class CarsFavorite extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars_favorite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'sub_id'], 'required'],
            [['car_id', 'sub_id', 'type_id'], 'integer'],
            [['created'], 'safe'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertsType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['sub_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscriber::className(), 'targetAttribute' => ['sub_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Car ID'),
            'sub_id' => Yii::t('app', 'Sub ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(AlertsType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSub()
    {
        return $this->hasOne(Subscriber::className(), ['id' => 'sub_id']);
    }
}
