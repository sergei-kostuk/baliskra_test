<?php

use app\components\Panel;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Car';
?>


<div class="row">
    <div class="col-sm-6">
        <?php Panel::begin(); ?>
        <div class="row">
            <div class="col-xs-7">
                <?php
                if (count($car->carsPhotos)) {
                    $url = 'https://bilasolur.is/CarImage.aspx?s=' . $car->company->api_id . '&c=' . $car->api_id . '&p=' . $car->carsPhotos[0]->photo_id . '&w=300';
                    echo Html::img($url, ['style' => 'width:100%;']);
                }
                ?>


            </div>
            <div class="col-xs-5">
                <h5><?= $car->title; ?></h5>
                <div>Verð <?= number_format($car->verd * 1000); ?></div>
                <div>Nýskráning <strong><?= $car->dyr; ?>/<?= $car->argerd; ?></strong></div>
                <div><strong><?= $car->eldsneyti0->name; ?></strong></div>
                <div>Akstur <strong><?= number_format($car->ekinn * 1000); ?> km</strong></div>
                <div>Næsta skoðun <?= $car->naestaskodun; ?></div>
                <div><?= $car->girartegund0->name; ?></div>
                <div>Litur <?= $car->litur0->name; ?></div>
                <hr/>

                <div style="font-size: 18px; font-weight: 400;">Verd <span
                            style="color: #5867dd; font-weight: bold;"><?= number_format($car->verd * 1000, null, null, '.'); ?> kr.</span>
                </div>


                <?php if ($car->aukahlutirath): ?>
                    <div><?= $car->aukahlutirath; ?></div>
                <?php endif; ?>

                <?php if ($car->skiptiodyrari): ?>
                    <div>Seljandi skoðar skipti á ódýrara</div>
                <?php endif; ?>

                <?php if ($car->skiptidyrari): ?>
                    <div>Seljandi skoðar skipti á dýrara</div>
                <?php endif; ?>

            </div>
        </div>


        <div class="row">
            <div class="col-xs-7">
                <div class="row">
                    <div class="col-xs-6">
                        <div>Skráð á söluskrá</div>
                        <div><strong><?= date("d.m.Y", strtotime($car->skrdags)); ?></strong></div>
                    </div>
                    <div class="col-xs-6">
                        <div>Síðast uppfært</div>
                        <div><strong><?= date("d.m.Y", strtotime($car->lastupdated)); ?></strong></div>
                    </div>
                </div>
                <div>Car status</div>
                <div>
                    <strong>
                        <?= $car->deleted == 0 ? 'Online' : 'offline'; ?>
                    </strong>
                </div>
            </div>
            <div class="col-xs-5">
                <?php if (!$car->deleted): ?>
                    <?php if ($car->company->api_id == 19): ?>
                        <b>Bi</b>
                        <a href="https://bilasolur.is/CarDetails.aspx?bid=<?= $car->company->api_id; ?>&cid=<?= $car->api_id; ?>&sid=<?= $car->ext_statid; ?>"
                           target="_blank"
                           class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn-sm m-btn--bolder">View
                            car
                            in browser</a>
                    <?php else: ?>
                        <a href="https://bilasolur.is/<?= $car->carsProps->url_bilasolur; ?>"
                           target="_blank"
                           class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn-sm m-btn--bolder">View
                            car
                            in browser</a>
                        <?= Html::a("Update Parser", ['/cars/update-parser', 'id' => $car->id]); ?>
                    <?php endif; ?>

                <?php endif; ?>
            </div>
        </div>
        <?php Panel::end(); ?>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <?php Panel::begin(); ?>
                graph 1
                <?php Panel::end(); ?>
            </div>
            <div class="col-sm-6">
                <?php Panel::begin(); ?>
                graph 2
                <?php Panel::end(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?php Panel::begin(); ?>
                graph 3
                <?php Panel::end(); ?>
            </div>
            <div class="col-sm-6">
                <?php Panel::begin(); ?>
                graph 4
                <?php Panel::end(); ?>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <?php Panel::begin(['title' => 'Active alerts']); ?>

        <?php
        $name = 'alerts_list';
        $column = [
            'id',
            'email',
            'method',
            [
                'header' => 'Activated',
                'value' => function ($d) {
                    $dStart = new DateTime($d->created);
                    $dEnd = new DateTime();
                    $dDiff = $dStart->diff($dEnd);
                    return $dDiff->days . " days";
                }
            ],
            [
                'header' => 'Lasr send',
                'value' => function ($d) {
                    $al = app\models\AlertsLog::find()->where(['alert_id' => $d->id])->orderBy(['id' => SORT_DESC])->one();
                    if ($al) {
                        return $al->created;
                    }
                }
            ],
            [
                'header' => 'Total send',
                'value' => function ($d) {
                    return count($d->alertsLogs);
                }
            ], [
                'contentOptions' => ['class' => 'm-datatable__cell'],
                'header' => 'Action',
                'format' => 'raw',
                'value' => function ($d) {
                    return $this->render('/base/action', ['model' => $d, 'action' => 'alerts']);
                }
            ]
        ];


        Pjax::begin();
        echo GridView::widget([
            'id' => $name,
            'dataProvider' => $dataProvider,
            'columns' => $column,
        ]);
        Pjax::end();
        ?>
        <?php Panel::end(); ?>
    </div>
    <div class="col-sm-4">
        <?php Panel::begin(['title' => 'Audit Log']); ?>
        c
        <?php Panel::end(); ?>
    </div>
</div>

