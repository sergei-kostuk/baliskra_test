<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lb_auth_item".
 *
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property LbAuthAssignment[] $lbAuthAssignments
 * @property LbUser[] $users
 * @property LbAuthRule $ruleName
 * @property LbAuthItemChild[] $lbAuthItemChildren
 * @property LbAuthItemChild[] $lbAuthItemChildren0
 * @property LbAuthItem[] $parents
 * @property LbAuthItem[] $children
 */
class LbAuthItem extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lb_auth_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'description'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => LbAuthRule::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('user', 'Name'),
            'type' => Yii::t('user', 'Type'),
            'description' => Yii::t('user', 'Description'),
            'rule_name' => Yii::t('user', 'Rule Name'),
            'data' => Yii::t('user', 'Data'),
            'created_at' => Yii::t('user', 'Created At'),
            'updated_at' => Yii::t('user', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbAuthAssignments()
    {
        return $this->hasMany(LbAuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('lb_auth_assignment', ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(LbAuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbAuthItemChildren()
    {
        return $this->hasMany(LbAuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbAuthItemChildren0()
    {
        return $this->hasMany(LbAuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(LbAuthItem::className(), ['name' => 'parent'])->viaTable('lb_auth_item_child', ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(LbAuthItem::className(), ['name' => 'child'])->viaTable('lb_auth_item_child', ['parent' => 'name']);
    }
}
