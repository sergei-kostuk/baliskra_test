<div class="m-portlet m-portlet--tab">
    <?php if ($title || ($buttons && !empty($buttons))): ?>
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                    <h3 class="m-portlet__head-text">
                        <?= $title; ?>
                    </h3>
                </div>
                <?php if($search_field):?>
                    <div style="margin-left: 15px">
                        <input type="text" class="form-control" value="<?=Yii::$app->request->get('q');?>" id="search" />
                    </div>
                <?php endif;?>
            </div>


            <?php if ($buttons || $table_name): ?>
                <div class="m-portlet__head-tools" id="data-grid-column-prop">
                    <ul class="m-portlet__nav">
                        <?php if(isset($buttons) && !empty($buttons)):?>
                            <?php foreach ($buttons as $button): ?>
                                <li class="m-portlet__nav-item">
                                    <a href="<?= \yii\helpers\Url::to($button['link']); ?>"
                                       class="-m-portlet__nav-link btn btn-success ---m-btn --m-btn--pill --m-btn--air">
                                        <?= $button['title']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ($table_name): ?>
                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#"
                                   class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn-lg--  btn-metal m-btn m-btn--pill">
                                    {{status}}
                                </a>
                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                  style="left: auto; right: 32.5px;"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav table-nav">
                                                    <li v-for="(column,index) in columns"
                                                        class="m-nav__section m-nav__section--first">
                                                        <label v-if="column.name" class="m-checkbox">
                                                            <input type="checkbox" value="{{column.value}}"
                                                                   checked="{{ column.value }}" v-model="column.value"/>
                                                            <span></span>
                                                            {{column.name}}
                                                        </label>
                                                    </li>

                                                    <!--                                        <li class="m-nav__separator m-nav__separator--fit">-->
                                                    <!--                                        </li>-->
                                                    <li class="m-nav__item hide">
                                                        <a href="#"
                                                           class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Apply</a>
                                                        <a href="#"
                                                           class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endif; ?>


        </div>
    <?php endif; ?>
    <div class="m-portlet__body">
        <?= $content; ?>
    </div>
</div>


<?php if ($table_name): ?>
    <script>
        document.addEventListener("DOMContentLoaded", function (d) {
            app = new Vue({
                el: '#data-grid-column-prop',
                data: {
                    name: '<?=$table_name;?>',
                    columns: getColumnName('<?=$table_name;?>'),
                    status: 'All'
                },
                mounted: function () {
                    this.setStatus();
                    hideColumn(getColumnName(this.name));
                },
                watch: {
                    columns: {
                        handler: function (val, oldVal) {
                            this.setStatus();
                            localStorage.setItem(this.name, JSON.stringify(val));
                            hideColumn(val);
                        }
                        ,
                        deep: true
                    }
                },
                methods: {
                    setStatus: function () {
                        console.log('setSt');
                        for (var k in this.columns) {
                            if (!this.columns[k].value) {
                                this.status = 'Custom';
                                return;
                            }
                        }
                        this.status = 'All';
                    },
                },
            });

            $("#search").keyup(function (e) {
                var q = $(this).val();
                console.log('q',q);
                $.pjax({
                    // type       : 'POST',
                    // url        : '/test/index',
                    container  : '#car-pjax',
                    data       : {q:q},
                    push       : true,
                    replace    : false,
                    timeout    : 10000,
                    "scrollTo" : false
                })
            });
        });


        function hideColumn(data) {
            $("#<?=$table_name;?> .table > thead th").each(function (i, e) {
                if (!data[i].value) {
                    $(e).hide();
                } else {
                    $(e).show();
                }
                // console.log("--",data[i]);
            });

            $("#<?=$table_name;?>  .table tr").each(function (ii, ee) {
                $(ee).find('td').each(function (i, e) {
                    if (!data[i].value) {
                        $(e).hide();
                    } else {
                        $(e).show();
                    }
                    // console.log("==",data[i]);
                });
            });
        }

        function getColumnName(name) {
            if (name) {
                if (localStorage.getItem(name)) {
                    var column = JSON.parse(localStorage.getItem(name));
                    console.log('----------------------',column.langth);
                    if(column.langth > 0){
                        return column;
                    }
                }
            }
            var column = [];
            console.log("#<?=$table_name;?>  .table > thead th");
            $("#<?=$table_name;?>  .table > thead th").each(function (i, e) {
                column.push({name: $(e).text(), value: 1});
            });
            console.log('column', column);
            return column;
        }


    </script>

<?php endif; ?>