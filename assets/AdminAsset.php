<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/admin';
    public $css = [
        'css/style.bundle.css',
        'css/vendors.bundle.css',
        'css/base.css'
    ];
    public $js = [
        'js/dashboard.js',
        'js/scripts.bundle.js',
        '../js/sweetalert.min.js',
        '../js/yii.confirm.overrides.js',

    ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
