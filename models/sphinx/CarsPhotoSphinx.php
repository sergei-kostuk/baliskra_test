<?php

namespace app\models\sphinx;

use Yii;

/**
 * This is the model class for index "cars_photo".
 *
 * @property integer $id
 * @property string $car_id
 * @property string $photo_id
 * @property string $so
 * @property string $created
 */
class CarsPhotoSphinx extends \yii\sphinx\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function indexName()
    {
        return 'cars_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id'], 'integer'],
            [['car_id', 'photo_id', 'so', 'created'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => 'Car ID',
            'photo_id' => 'Photo ID',
            'so' => 'So',
            'created' => 'Created',
        ];
    }
}
