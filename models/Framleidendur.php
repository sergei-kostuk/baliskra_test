<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "framleidendur".
 *
 * @property int $id
 * @property int $model_id
 * @property string $company
 * @property string $models
 * @property int $from_cars
 *
 * @property Cars[] $cars
 */
class Framleidendur extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'framleidendur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'model_id', 'from_cars'], 'integer'],
            [['company', 'models'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'model_id' => Yii::t('user', 'Model ID'),
            'company' => Yii::t('user', 'Company'),
            'models' => Yii::t('user', 'Models'),
            'from_cars' => Yii::t('user', 'From Cars'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Cars::className(), ['ext_modframleidandiid' => 'id']);
    }
}
