<?php

namespace app\models\sphinx;

use Yii;

/**
 * This is the model class for index "cat".
 *
 * @property integer $id
 * @property string $name
 */
class CategoriesSphinx extends \yii\sphinx\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function indexName()
    {
        return 'cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id'], 'integer'],
            [['name'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
