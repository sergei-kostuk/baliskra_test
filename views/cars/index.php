<?php
$this->title = 'Cars';

use app\components\Panel;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>


<div class="row">
    <div class="col-md-12">

<?php
$name = 'cars_list';
Panel::begin([
    'title' => $this->title,
//    'buttons' => [['title' => 'Add New', 'link' => '/companies/add']],
    'table_name' => $name,
    'search_field' => true
]);
?>
        <?php
        //        $column = [
        //            'id',
        //            'eigandi',
        //            'flokkur',
        //            'argerd',
        //            'dyr',
        //            'manna',
        //            'ekinn',
        //            'litur',
        //            'drif',
        //            'girar',
        //            'eldsneyti',
        //            'sumardekk',
        //            'vetrardekk',
        //            'heilsarsdekk',
        //            'lowprofiledekk',
        //            'slagrymi',
        //            'aukahlutirath',
        //            'skiptiodyrari',
        //            'skiptiodyrarikr',
        //            'skiptidyrari',
        //            'skiptidyrarikr',
        //            'skiptiath',
        //            'naestaskodun',
        //            'skrdags',
        //            'verd',
        //            'bilalan',
        //            'bilalanupphaed',
        //            'lanveitandiid',
        //            'bilalanafborgun',
        //            'dekkjastaerd',
        //            'hestofl',
        //            'hestofl',
        //            'strokkar',
        //            'tyngd',
        //            'fyrstiskrdagur',
        //            'spot',
        //            'verdath',
        //            'verdtilbod',
        //            'skrmanudur',
        //            'modelar',
        //            'lastupdated',
        //            'aukahlutir',
        //            'akeiningar',
        //            'bilalanlokagreidsla',
        //            'bilalanlanstimi',
        //            'burdargeta',
        //            'tjonabifreid',
        //            'stadsetning',
        //            'nytt',
        //            'innflteg',
        //            'felgustaerd',
        //            'spennarafkerfis',
        //            'eftirafdekkjum',
        //            'oxlafjoldi',
        //            'nytimareim',
        //            'timareimkm',
        //            'anvsk',
        //            'stadsettpnr',
        //            'flokkur2',
        //            'flokkur3',
        //            'eldsneyti2',
        //            'eydslainnanb',
        //            'eydslautanb',
        //            'eydslablandad',
        //            'co2',
        //            'skrath',
        //            'fjlykla',
        //            'fjlyklaremote',
        //            'timabunadur',
        //            'girartegund',
        //            'solulysing',
        //            'thskodun',
        //            'thskodunkm',
        //            'thskodundags',
        //            'batterykw',
        //            'batterykm',
        //            'verdadurkr',
        //            'ext_imagecount',
        //            'ext_modframleidandi',
        //            'ext_modframleidandiid',
        //            'ext_modgerd',
        //            'ext_modgerdid',
        //            'ext_ekinnkm',
        //            'ext_argerd',
        //            'ext_statid',
        //            'ext_sortkey',
        //            'deleted',
        //            [
        //                'attribute' => 'deleted_at',
        //                'value' => function ($d) {
        //                    if ($d->deleted_at) {
        //                        return date("Y-m-d H:i", $d->deleted_at);
        //                    }
        //                }
        //            ]
        //        ];


        $column = [
            [
                'attribute'=>'api_id',
//                'header' => 'Car ID',
                'format' => 'raw',
                'value' => function ($d) {
                    return \yii\helpers\Html::a($d->api_id, '/cars/' . $d->id, ['data-pjax' => 0]);
                }
            ],
            'ext_modframleidandi',
//            'extModframleidandi.models',
            'model_name',
            [
                'attribute'=>'verd',
//                'header' => 'Price',
                'value' => function ($d) {
                    return number_format($d->verd * 1000) . " kr";
                }
            ],
            'discount_price',

            [
                'label' => 'On Website',
                'attribute' => 'created',
                'format' => ['date', 'php:Y-m-d'],
            ],

            [
                'label' => 'Page Views',
                'attribute' => 'page_views',
            ],

            'in_search',
            'contact_clicks',
            'alerts',
            [
                'contentOptions' => ['class' => 'm-datatable__cell table-action-cell'],
                'header' => 'Action',
                'format' => 'raw',
                'value' => function ($d) {
                    return $this->render('/base/action', ['model' => $d, 'action' => 'cars']);
                }
            ]
        ];

        Pjax::begin([
                'id'=>'car-pjax'
        ]);
        echo GridView::widget([
            'id' => $name,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,
        ]);

        Pjax::end();
        ?>
        <?php Panel::end(); ?>
    </div>
</div>
