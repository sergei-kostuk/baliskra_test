<?php

namespace app\models;

use Yii;
use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;

/**
 * This is the model class for table "alerts".
 *
 * @property int $id
 * @property string $email
 * @property int $subscriber_id
 * @property int $method
 * @property int $type_id
 * @property int $car_id
 * @property int $deleted
 * @property string $created
 * @property string $updated
 *
 * @property AlertsType $type
 * @property AlertsLog[] $alertsLogs
 */
class Alerts extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alerts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscriber_id', 'type_id'], 'required'],
            [['subscriber_id', 'method', 'type_id', 'car_id', 'deleted'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['email'], 'string', 'max' => 150],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertsType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'subscriber_id' => Yii::t('app', 'Subscriber ID'),
            'method' => Yii::t('app', 'Method'),
            'type_id' => Yii::t('app', 'Type ID'),
            'car_id' => Yii::t('app', 'Car ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(AlertsType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertsLogs()
    {
        return $this->hasMany(AlertsLog::className(), ['alert_id' => 'id']);
    }

    public function sendEmail($tpl, $subject, $params = [])
    {
        $res = Yii::$app->mailer->compose($tpl, $params)
            ->setFrom('zp.dima@gmail.com')
            ->setTo($this->subscriber->email)
            ->setSubject($subject)
            ->send();


        return $res;
    }

    public function sendPuth($title, $massage, $link)
    {
        $auth = array(
            //'GCM' => 'AIzaSyB_TdmuATGFu2a3O-3Ikx7Yzk69ayJhZRc', // deprecated and optional, it's here only for compatibility reasons
            'VAPID' => array(
                'subject' => 'mailto:zp.dima@gmail.com',
                'publicKey' => 'BGT5QsKDXKmChRoLTvbBeRSIc-V2tupGjgeO8W-5ryhE1dLk1wwW226p9v2FNlmBg8S4FvKRvrgrBfkogsOH7MI', // got from https://web-push-codelab.appspot.com/
                'privateKey' => '8Oww6v9nv22WVe41leBBtE55zFsqiOowkNzu3Ey67WI', // got from https://web-push-codelab.appspot.com/
            ),
        );


        $data = ['m' => $massage, 'l' => $link, 't' => $title];
        $notifications = [
            [
                'subscription' => Subscription::create([
                    'endpoint' => 'https://fcm.googleapis.com/fcm/send/fLxF_jIlNNs:APA91bHfgJFjgnCNFm-5XsyHrq7SuMhzFLnlaRHS6vjEYAEU7vH6q5C4bwD6zT0fbJti5gw_RJwkfg6x0SXCfuRZvljmCbw33QnryDaxNwj51ttNdPnt2_g61B1WMxPKinFbC3j5Pt9sFayWTa0LFe-fm3uBKxDXlA',
                    'publicKey' => 'BE-Jihal316b5CgjBoAB7kKCQcnTAClscDDTELMVRR6-BWf7ThnDWyJRW8X-U18xksLeC_6x3uYaTklVTa_rFnQ', // base 64 encoded, should be 88 chars
                    'authToken' => 'r-EACY4J3jdGX81X_db8fg', // base 64 encoded, should be 24 chars
                ]),
                'payload' => json_encode($data),
            ]
        ];

        $webPush = new WebPush($auth);

        $res = $webPush->sendNotification(
            $notifications[0]['subscription'],
            $notifications[0]['payload'], // optional (defaults null)
            true // optional (defaults false)
        );
    }

}
