<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lb_city".
 *
 * @property int $id
 * @property int $country_id
 * @property string $city
 * @property string $state
 * @property string $region
 * @property int $biggest_city
 *
 * @property LbCountry $country
 * @property LbUser[] $lbUsers
 */
class LbCity extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lb_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'city', 'region'], 'required'],
            [['country_id', 'biggest_city'], 'integer'],
            [['city', 'state', 'region'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => LbCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'city' => 'City',
            'state' => 'State',
            'region' => 'Region',
            'biggest_city' => 'Biggest City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(LbCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbUsers()
    {
        return $this->hasMany(LbUser::className(), ['city_id' => 'id']);
    }
}
