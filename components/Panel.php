<?php

namespace app\components;

use yii\base\Widget;


class Panel extends Widget
{
    public $title;
    public $buttons;
    public $filter = false;
    public $table_name = '';
    public $search_field = false;

    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        // you can load & return the view or you can return the output variable
        $content = ob_get_clean();

        if (!trim($content)) {
            return '';
        }

        return $this->render('panel', [
            'title' => $this->title,
            'buttons' => $this->buttons,
            'content' => $content,
            'filter' => $this->filter,
            'table_name' => $this->table_name,
            'search_field' => $this->search_field
        ]);
    }
}

?>