<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars_view_key".
 *
 * @property int $id
 * @property string $key
 * @property string $ip
 * @property string $user_agent
 * @property string $created
 *
 * @property CarsView[] $carsViews
 */
class CarsViewKey extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars_view_key';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['created'], 'safe'],
            [['key'], 'string', 'max' => 50],
            [['ip'], 'string', 'max' => 25],
            [['user_agent'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'key' => Yii::t('user', 'Key'),
            'ip' => Yii::t('user', 'Ip'),
            'user_agent' => Yii::t('user', 'User Agent'),
            'created' => Yii::t('user', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsViews()
    {
        return $this->hasMany(CarsView::className(), ['key_id' => 'id']);
    }
}
