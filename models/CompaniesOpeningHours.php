<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies_opening_hours".
 *
 * @property int $id
 * @property int $company_id
 * @property int $day_week
 * @property string $start
 * @property string $end
 * @property int $close
 *
 * @property DaysWeek $dayWeek
 * @property Companies $company
 */
class CompaniesOpeningHours extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies_opening_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'day_week', 'start', 'end', 'close'], 'required'],
            [['company_id', 'day_week', 'close'], 'integer'],
            [['start', 'end'], 'safe'],
            [['day_week'], 'exist', 'skipOnError' => true, 'targetClass' => DaysWeek::className(), 'targetAttribute' => ['day_week' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'day_week' => 'Day Week',
            'start' => 'Start',
            'end' => 'End',
            'close' => 'Close',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDayWeek()
    {
        return $this->hasOne(DaysWeek::className(), ['id' => 'day_week']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
