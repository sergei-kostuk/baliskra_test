<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars_contact_form".
 *
 * @property int $id
 * @property int $car_id
 * @property string $name
 * @property string $email
 * @property string $created
 *
 * @property Cars $car
 */
class CarsContactForm extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars_contact_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id'], 'integer'],
            [['created'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'car_id' => Yii::t('user', 'Car ID'),
            'name' => Yii::t('user', 'Name'),
            'email' => Yii::t('user', 'Email'),
            'created' => Yii::t('user', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }
}
