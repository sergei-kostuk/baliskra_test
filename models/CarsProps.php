<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars_props".
 *
 * @property int $id
 * @property int $car_id
 * @property int $views
 * @property int $alerts
 * @property int $contacts_clicks
 * @property int $in_search
 * @property string $created
 * @property string $updated
 *
 * @property Cars $car
 */
class CarsProps extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars_props';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id'], 'required'],
            [['car_id', 'views', 'alerts', 'contacts_clicks', 'in_search'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'car_id' => Yii::t('user', 'Car ID'),
            'views' => Yii::t('user', 'Views'),
            'alerts' => Yii::t('user', 'Alerts'),
            'contacts_clicks' => Yii::t('user', 'Contacts Clicks'),
            'in_search' => Yii::t('user', 'In Search'),
            'created' => Yii::t('user', 'Created'),
            'updated' => Yii::t('user', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }
}
