<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alerts_log".
 *
 * @property int $id
 * @property int $alert_id
 * @property int $status
 * @property int $opened
 * @property string $hash
 * @property string $created
 *
 * @property Alerts $alert
 */
class AlertsLog extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alerts_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alert_id', 'status'], 'required'],
            [['alert_id', 'status', 'opened'], 'integer'],
            [['created'], 'safe'],
            [['hash'], 'string', 'max' => 60],
            [['alert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alerts::className(), 'targetAttribute' => ['alert_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'alert_id' => Yii::t('user', 'Alert ID'),
            'status' => Yii::t('user', 'Status'),
            'opened' => Yii::t('user', 'Opened'),
            'hash' => Yii::t('user', 'Hash'),
            'created' => Yii::t('user', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlert()
    {
        return $this->hasOne(Alerts::className(), ['id' => 'alert_id']);
    }
}
