<?php
$this->title = 'Add Cars';

use app\components\Panel;
use yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
?>

<div>
    <div class="h">
        <h1>Add new Car</h1>
    </div>


    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-9',
            ],
        ],
    ]); ?>
    <div class="col-xs-12 col-md-4">
        <?php Panel::begin(); ?>
        <?= $form->field($cars, 'ext_modframleidandiid')->dropDownList(ArrayHelper::map(\app\models\Framleidendur::find()->groupBy('id')->all(),'id','company')); ?>
        <?= $form->field($cars, 'flokkur')->dropDownList(ArrayHelper::map(\app\models\Flokkar::find()->all(),'id','name'));; ?>
        <?= $form->field($cars, 'litur')->dropDownList(ArrayHelper::map(\app\models\Litir::find()->all(),'id','name')); ?>
        <?= $form->field($cars, 'drif'); ?>
        <?= $form->field($cars, 'eldsneyti')
            ->dropDownList(ArrayHelper::map(\app\models\Eldsneyti::find()->all(),'id','name'));; ?>
        <?php Panel::end(); ?>
    </div>

    <div class="col-xs-12 col-md-4">
        <?php Panel::begin(); ?>

        <?php Panel::end(); ?>
    </div>

    <div class="col-xs-12 col-md-4">
        <?php Panel::begin(); ?>

        <?php Panel::end(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
