<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property string $name
 * @property string $location
 * @property string $logo
 * @property string $phone
 * @property string $site
 * @property string $email
 * @property string $api_key
 * @property int $deleted
 * @property string $created
 * @property string $updated
 *
 * @property CompaniesOpeningHours[] $companiesOpeningHours
 * @property CompaniesUser[] $companiesUsers
 */
class Companies extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'location'], 'required'],
            [['deleted'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name', 'location', 'phone', 'site', 'email'], 'string', 'max' => 150],
            [['api_key'], 'string', 'max' => 200],
            [['logo'],'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Name'),
            'location' => Yii::t('user', 'Location'),
            'logo' => Yii::t('user', 'Logo'),
            'phone' => Yii::t('user', 'Phone'),
            'site' => Yii::t('user', 'Site'),
            'email' => Yii::t('user', 'Email'),
            'api_key' => Yii::t('user', 'Api Key'),
            'deleted' => Yii::t('user', 'Deleted'),
            'created' => Yii::t('user', 'Created'),
            'updated' => Yii::t('user', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesOpeningHours()
    {
        return $this->hasMany(CompaniesOpeningHours::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesUsers()
    {
        return $this->hasMany(CompaniesUser::className(), ['company_id' => 'id']);
    }
}
