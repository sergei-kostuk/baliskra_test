<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\Expression;

class CarsSearch extends Cars
{
    public function rules()
    {
        return [
            [['api_id', 'verd', 'ext_modframleidandi','model_name','discount_price','on_website','page_views','on_website','in_search','contact_clicks','alerts'], 'safe']
        ];
    }

    public function search($params)
    {
        $pageViewsSelect = CarsView::find()
            ->select(['count(*)'])
            ->where('car_id = `cars`.`id`')
        ;

        $discountSelect = History::find()
            ->alias('h')
            ->select(['h.old_value'])
            ->where('h.item_id = `cars`.`id` AND h.model_id = 1')
            ->limit(1)
            ->orderBy('h.created DESC')
        ;

        $query = Cars::find()
            ->distinct()
            ->select([
                'cars.*',
                'alerts' => 'a.id',
                'page_views' => $pageViewsSelect,
                'discount_price' => new Expression('IF(cars.verdtilbod, (' . $discountSelect->createCommand()->getRawSql() . '), 0)'),
            ])
            ->andWhere('company_id is not null')
            ->leftJoin(['a' => Alerts::tableName()], 'a.car_id = cars.id', ['type_id' => [2, 4]])
            ->joinWith(['extModframleidandi'])
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'ext_modframleidandi',
                    'api_id',
                    'verd',
                    'created',
                    'page_views',
                    'alerts',
                    'discount_price',
                    'model_name'
                ], 'defaultOrder' => [
                    'api_id' => SORT_DESC
                ]
            ]
        ]);

        /**
         * Настройка параметров сортировки
         * Важно: должна быть выполнена раньше $this->load($params)
         */
//        $dataProvider->setSort([
//            'attributes' => [
//                'ext_modframleidandi',
//                'api_id',
//                'verd',
//                'framleidendur.models'
//            ], 'defaultOrder' => [
//                'api_id' => SORT_DESC
//            ]
//        ]);







        $dataProvider->sort->attributes['model_name'] = [
            'asc' => ['framleidendur.models' => SORT_ASC],
            'desc' => ['framleidendur.models' => SORT_DESC],
        ];

        if (isset($params['q']) && $params['q']) {
            $q = $params['q'];

            $query->andFilterWhere(['like', 'api_id', $q . '%', false]);
            $query->orFilterWhere(['like', 'verd', $q . '%', false]);
            $query->orFilterWhere(['like', 'framleidendur.models', $q . '%', false]);

            return $dataProvider;
        }

        $this->load($params);
        $query->andFilterWhere(['like', 'api_id', $this->api_id]);
        $query->andFilterWhere(['like', 'ext_modframleidandi', $this->ext_modframleidandi]);
        $query->andFilterWhere(['like', 'framleidendur.models', $this->model_name]);
        $query->andFilterWhere(['like', 'verd', $this->verd]);
//        $query->andFilterWhere(['like', 'discount_price', $this->verd]);


        return $dataProvider;
    }
}