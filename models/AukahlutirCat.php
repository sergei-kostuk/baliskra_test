<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aukahlutir_cat".
 *
 * @property int $id
 * @property string $name
 *
 * @property Aukahlutir[] $aukahlutirs
 */
class AukahlutirCat extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aukahlutir_cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAukahlutirs()
    {
        return $this->hasMany(Aukahlutir::className(), ['cat' => 'id']);
    }
}
