<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<div>
    <a href="<?= Url::to($action . "/edit/" . $model->id, true); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View ">
        <i class="la la-edit"></i>
    </a>

    <?=
    Html::a('<i class="la la-trash"></i>', Url::to($action . "/" . $model->id, true), [
        'class' => "m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill",
        'title' => 'Delete',
        'data' => [
            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
            'method' => 'DELETE',
        ]
    ]);
    ?>
</div>

