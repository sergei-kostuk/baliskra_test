<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisement_size".
 *
 * @property int $id
 * @property int $width
 * @property int $height
 *
 * @property Advertisement[] $advertisements
 */
class AdvertisementSize extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement_size';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'width', 'height'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'width' => Yii::t('user', 'Width'),
            'height' => Yii::t('user', 'Height'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['size_id' => 'id']);
    }
}
