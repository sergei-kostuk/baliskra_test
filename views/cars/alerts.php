<?php
$this->title = 'Cars Alerts';

use yii\grid\GridView;
use app\components\Panel;
use yii\widgets\Pjax;
?>




<div class="row">
    <div class="col-md-12">

        <?php
        $name = 'cars_list';
        Panel::begin([
            'title' => $this->title,
//    'buttons' => [['title' => 'Add New', 'link' => '/companies/add']],
            'table_name' => $name
        ]);
        ?>
        <?php
        $column = [
            [
                'attribute'=>'api_id',
                'format' => 'raw',
                'value' => function ($d) {
                    return \yii\helpers\Html::a($d->api_id, '/cars/' . $d->id, ['data-pjax' => 0]);
                }
            ],
            'ext_modframleidandi',
            'extModframleidandi.models',
            [
                'attribute'=>'verd',
                'value' => function ($d) {
                    return number_format($d->verd * 1000) . " kr";
                }
            ], [
                'header' => 'Discount Price',
                'value' => function ($d) {
                    return number_format($d->verd * 1000) . " kr";
                }
            ], [
                'header' => 'On website Since',
                'value' => function ($d) {
                    $dStart = new DateTime($d->created);
                    $dEnd = new DateTime();
                    $dDiff = $dStart->diff($dEnd);
                    return $dDiff->days . " days";
                }
            ], [
                'header' => 'In search',
                'value' => function ($d) {

                }
            ], [
                'header' => 'Page views',
                'value' => function ($d) {

                }
            ], [
                'header' => 'Contact clicks',
                'value' => function ($d) {

                }
            ], [
                'header' => 'Alerts',
                'value' => function ($d) {

                }
            ], [
                'contentOptions' => ['class' => 'm-datatable__cell table-action-cell'],
                'header' => 'Action',
                'format' => 'raw',
                'value' => function ($d) {
                    return $this->render('/base/action', ['model' => $d, 'action' => 'cars']);
                }
            ]
        ];

        Pjax::begin([
            'id'=>'car-pjax'
        ]);
        echo GridView::widget([
            'id' => $name,
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => $column,
        ]);

        Pjax::end();
        ?>
        <?php Panel::end(); ?>
    </div>
</div>