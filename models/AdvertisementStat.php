<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisement_stat".
 *
 * @property int $id
 * @property int $ad_id
 * @property int $in_screen
 * @property int $loads
 * @property int $clicks
 * @property string $created
 * @property string $updated
 *
 * @property Advertisement $ad
 */
class AdvertisementStat extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement_stat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_id'], 'required'],
            [['ad_id', 'in_screen', 'loads', 'clicks'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['ad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertisement::className(), 'targetAttribute' => ['ad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'ad_id' => Yii::t('user', 'Ad ID'),
            'in_screen' => Yii::t('user', 'In Screen'),
            'loads' => Yii::t('user', 'Loads'),
            'clicks' => Yii::t('user', 'Clicks'),
            'created' => Yii::t('user', 'Created'),
            'updated' => Yii::t('user', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAd()
    {
        return $this->hasOne(Advertisement::className(), ['id' => 'ad_id']);
    }
}
