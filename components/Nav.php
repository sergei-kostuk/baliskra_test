<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class Nav extends Widget {
    public $items;

    public function init() {
        // your logic here
        parent::init();
    }

    public function run() {
        // you can load & return the view or you can return the output variable
        return $this->render('@app/components/views/nav', ['items' => $this->items]);
    }
}

?>