<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flokkar".
 *
 * @property int $id
 * @property string $name
 *
 * @property VehicleTypeSub[] $vehicleTypeSubs
 */
class Flokkar extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flokkar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleTypeSubs()
    {
        return $this->hasMany(VehicleTypeSub::className(), ['flokkar_id' => 'id']);
    }
}
