<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class BaseAdmin extends Controller
{
    public $layout = 'admin';

    public function setAlert($title, $type = 'success')
    {
        Yii::$app->session->setFlash($type, $title);
    }
}
