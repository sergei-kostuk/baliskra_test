<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisement_placement".
 *
 * @property int $advertisement_id
 * @property int $advertisement_placement_id
 *
 * @property Advertisement $advertisement
 * @property AdvertisementPlacementName $advertisementPlacement
 */
class AdvertisementPlacement extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement_placement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['advertisement_id', 'advertisement_placement_id'], 'integer'],
            [['advertisement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertisement::className(), 'targetAttribute' => ['advertisement_id' => 'id']],
            [['advertisement_placement_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvertisementPlacementName::className(), 'targetAttribute' => ['advertisement_placement_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'advertisement_id' => Yii::t('user', 'Advertisement ID'),
            'advertisement_placement_id' => Yii::t('user', 'Advertisement Placement ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisement()
    {
        return $this->hasOne(Advertisement::className(), ['id' => 'advertisement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisementPlacement()
    {
        return $this->hasOne(AdvertisementPlacementName::className(), ['id' => 'advertisement_placement_id']);
    }
}
