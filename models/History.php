<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property int $model_id
 * @property int $item_id
 * @property string $column
 * @property string $old_value
 * @property string $created
 *
 * @property HistoryModels $model
 */
class History extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'item_id', 'column', 'old_value'], 'required'],
            [['model_id', 'item_id'], 'integer'],
            [['old_value'], 'string'],
            [['created'], 'safe'],
            [['column'], 'string', 'max' => 50],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => HistoryModels::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'item_id' => 'Item ID',
            'column' => 'Column',
            'old_value' => 'Old Value',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(HistoryModels::className(), ['id' => 'model_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        if ($this->model_id == 1) {
            $car = Cars::findOne($this->item_id);

//            if ($this->old_value > $car->verd) {
//                $alerts = Alerts::find()->joinWith('subscriber')->where(['car_id' => $this->item_id, 'subscriber.status' => 1])->all();
//                foreach ($alerts as $alert) {
//                    $log = new AlertsLog();
//                    $log->hash = md5($alert->id . time());
//
//
//                    $res = $alert->sendEmail('@app/mail/car_price_drop', strip_tags($car->title) . ' Change price', [
//                        'car' => $car,
//                        'old_price' => $this->old_value,
//                        'hash' => $log->hash
//                    ]);
//
//                    if ($res) {
//                        echo " Email Send ";
//                    }
//
//
//                    $log->alert_id = $alert->id;
//                    $log->status = $res;
//
//                    if (!$log->save()) {
//                        print_r($log->getErrors());
//                        die;
//                    }
//                }
//            }
        }
    }
}
