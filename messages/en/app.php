<?php

return [
    'brands' => 'Leitarskilyrði',
    'brand_model' => 'Gerð',
    'fuel_type' => 'Eldsneyti',
    'argerd' => 'árgerð',
    'verd' => 'verð',
    'ekinn' => 'ekinn',
    'girartegund' => 'Skipting',
    'other' => 'Aukahlutir, annað:'
];