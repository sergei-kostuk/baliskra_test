<?php

namespace app\models;

use app\models\sphinx\CategoriesSphinx;
use yii\helpers\ArrayHelper;
use app\models\sphinx\CarsSphinx;
use yii\helpers\Url;
use yii\sphinx\MatchExpression;
use Yii;

//use app\models\Cars as CarsSphinx;

class CarsFilter
{
    public static $data;

    public static $select = [
        'id',
        'api_id',
        'ext_modframleidandi as b',
        'ext_modgerd as m',
        'eldsneyti as ft',
        'IF(eldsneyti2,eldsneyti2, 1000) as ft2',
        'argerd as a',
        'verd as v',
        '`ekinn` * 1000 as e',
        'girartegund as g',
        'skrmanudur as sk',
        'ekinn as ek',
        'spot',
        'verdtilbod',
        'cid',
        'old_price as op',
        'main_img'
    ];

//    public static function setDiscount($arr)
//    {
//        foreach ($arr as $k => $item) {
//            if ($item['verdtilbod']) {
//                $oldPrice = History::find()->where(['item_id' => $item['id']])->orderBy(['id' => SORT_DESC])->one();
//                $arr[$k]['op'] = 0;
//                if ($oldPrice) {
//                    $oldPrice = $oldPrice->old_value;
//                    $arr[$k]['op'] = $oldPrice;
//                }
//            }
//        }
//        return $arr;
//    }

    public static function cleanResData($data)
    {
        if (!self::cleanParams() && !file_exists('main_cars_filter.txt')) {
            file_put_contents('main_cars_filter.txt',serialize($data));
        }
        return $data;
        if(self::post('all')){
            unset($_SESSION['cars_filter']);
        }

        $data = unserialize(serialize($data));

        if(isset($_SESSION['cars_filter'])){
            $res = [];
            $session_data = unserialize($_SESSION['cars_filter']);
            foreach ($data as $k => $v) {
                if ($v != $session_data[$k]) {
                    $res[$k] = $v;
                }
            }

            $_SESSION['cars_filter'] = serialize($data);
            return $res;
        }
        $_SESSION['cars_filter'] = serialize($data);
        return $data;
    }

    public static function get($params)
    {

        $time = microtime(true);
        if (!self::cleanParams() && file_exists('./main_cars_filter.txt')) {
            return self::cleanResData(unserialize(file_get_contents('./main_cars_filter.txt')));
        }
        $brands = json_decode(file_get_contents("brands.json"));


        $categories = ArrayHelper::index(json_decode(file_get_contents("cat.json")), 'id');
        $vehicle_type = VehicleType::find()->all();

        $cars = [];
        $result = [];
        $resultCount = 0;
//        $sql = '';

        if (self::cleanParams()) {
            $query = self::getModel();
            $query->select(self::$select);

            $query->limit(30);


            self::setFilter($query);

            $q2 = clone $query;
            $resultCount = $q2->count();

            if(self::post('lid')){
                $query->andWhere(['>','id',self::post('lid')]);
            }
            $result = $query->asArray()->all();
//            $sql = $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
//            $result = self::setDiscount($result);
        } else {
            foreach ($vehicle_type as $vt) {
                $vt_sub = VehicleTypeSub::find()->where(['vehicle_type_id' => $vt->id])->all();

                $query = self::getModel()->where(['flokkur' => ArrayHelper::getColumn($vt_sub, 'flokkar_id')]);
                $query->select(self::$select);
                $query->orderBy(['lastupdated' => SORT_DESC]);


                $query->limit(10);

                self::setFilter($query);


                $res = $query->asArray()->all();
//                $res = self::setDiscount($res);
                self::setFav($res);
                foreach ($res as $k => $r) {
                    if (isset($r['p']) && $r['p']) {
                        $p = explode(",", $r['p']);
                        $res[$k]['p'] = $p[0];
                    }
                }


                if (count($res) > 0) {
                    $cars[$vt->id] = $res;
                }
            }
            $resultCount = CarsSphinx::find()->where(['deleted' => 0])->count();
        }
        $fuel_type = Eldsneyti::find()->asArray()->all();


        $query = self::getModel();

        $query->select(['eldsneyti', 'count(*) as co']);
        $query->groupBy('eldsneyti');
        self::setFilter($query, true);
        $r = $query->asArray()->all();

        $r = ArrayHelper::index($r, 'eldsneyti');

        foreach ($fuel_type as $k => $type) {
            $fuel_type[$k]['co'] = 0;
            if (isset($r[$type['id']])) {
                $fuel_type[$k]['co'] = $r[$type['id']]['co'];
            }
        }

        $json = file_get_contents(\yii\helpers\Url::to('@app/filter.json'));
        $json = json_decode($json);


        $query = self::getModel();
        $query->select(['girartegund as g', 'count(*) as co']);
        $query->groupBy('g');
        self::setFilter($query, true);

        $r = $query->asArray()->all();
        $girartegund = ArrayHelper::index($r, 'g');


        $vt = ArrayHelper::getColumn(VehicleTypeSub::find()->where(['vehicle_type_id' => self::getData('vt')])->all(), 'flokkar_id');
        $flokkar = Flokkar::find()
            ->where([Flokkar::tableName() . '.id' => $vt])
            ->select([VehicleTypeSub::tableName() . '.vehicle_type_id as vid', Flokkar::tableName() . '.id', Flokkar::tableName() . '.name'])
            ->joinWith(['vehicleTypeSubs'])
            ->asArray()->all();

        $cat = ArrayHelper::index(AukahlutirCat::find()->asArray()->all(), 'id');
        $query = Aukahlutir::find();

        $query->where(['is not', 'cat', null]);

        $other = [];


        foreach ($query->asArray()->all() as $item) {
            $c = $item['cat'];
            unset($item['cat']);
            $other[$cat[$c]['name']][] = $item;
        }

        $sub = Subscriber::getSubscriber();
        $historySearch = null;
        $d = self::cleanParams(self::getData('all')['filters']);
        if ($sub) {
            $historySearch = HistorySearch::find()->where(['params' => json_encode($d), 'user_id' => $sub->id, 'type_id' => 2])->one();
        }
        if (!Yii::$app->user->isGuest) {
            $historySearch = HistorySearch::find()->where(['params' => json_encode($d), 'user_id' => Yii::$app->user->getId(), 'type_id' => 4])->one();
        }


        $end = microtime(true) - $time;

        return self::cleanResData([
            'brands' => $brands,
            'brand_model' => self::getBrandsModel(),
            'categories' => ArrayHelper::getColumn($categories, 'name'),
            'cars' => $cars,
            'post' => $end,
            'fuel_type' => $fuel_type,
            'argerd' => $json->argerd,
            'verd' => $json->verd,
            'ekinn' => $json->ekinn,
            'girartegund' => $girartegund,
            'flokkar' => $flokkar,
            'other' => $other,
            'vt' => $vehicle_type,
            'result' => $result,
            'resultCount' => $resultCount,
            'isSub' => $historySearch ? $historySearch->id : 0,
//            'sub' => $sub,
            'json' => $json,
//            'user' => Yii::$app->user->identity,
//            'sql' => $sql
        ]);
    }


    function setFav(&$cars)
    {
        $sub = Subscriber::getSubscriber();
        if ($sub) {
            foreach ($cars as $k => $car) {
                $cars[$k]['fav'] = CarsFavorite::find()->where(['car_id' => $car['id']])
                    ->andWhere(['sub_id' => ArrayHelper::getColumn(Subscriber::find()->where(['email' => $sub->email])->all(), 'id')])->count();
            }
        }
    }

    private static function getModel()
    {
        return Cars::find();
        $data = self::getData('other');

        $brand_model = self::getData('brand_model');
        if (!empty($data) || in_array(0, $brand_model)) {
            return Cars::find();
        }

        return CarsSphinx::find();
    }

    public static function getBrandsModel($all = false)
    {
        $brand_model = [];

        $brands_tmp = [];
        if ($all) {
            $r = Framleidendur::find()->all();
        } else {
            $r = Framleidendur::find()->where(['id' => CarsFilter::getData('brands')])->orderBy('models')->all();
        }
        $brand_id = null;
        foreach ($r as $item) {
            if ((!$brand_id || $brand_id != $item->id) && !isset($brands_tmp[$item->id])) {
                $brand_id = $item->id;

                $brands_tmp[$item->id][] = [
                    'name' => $item->company,
                    'id' => (string)$item->id,
                    'disable' => 1
                ];
            }
            $brands_tmp[$item->id][] = [
                'name' => $item->models,
                'bid' => (string)$item->id,
                'id' => "b" . (string)$item->model_id
            ];
        }


        foreach ($brands_tmp as $items) {
            foreach ($items as $item) {
                $brand_model[] = $item;
            }
        }
//        }
        return $brand_model;
    }

    public static function cleanParams($params = [])
    {
        if (empty($params) && !empty(CarsFilter::getData('all'))) {
            $params = CarsFilter::getData();
        }
        $json = json_decode(file_get_contents(Url::to('@app/filter.json')));
        $res = [];

        foreach ($params as $key => $item) {
            if (!empty($item)) {
                if ($key == 'argerd') {
                    if ($json->argerd->mi == $item[0] && $json->argerd->ma == $item[1]) {
                        continue;
                    }
                }

                if ($key == 'ekinn') {
                    if ($json->ekinn->mi == $item[0] && $json->ekinn->ma == $item[1]) {
                        continue;
                    }
                }

                if ($key == 'verd') {
                    if ($json->verd->mi == $item[0] && $json->verd->ma == $item[1]) {
                        continue;
                    }
                }
                $res[$key] = $item;
            }
            if ($key == 'verdtilbod' && $item === true) {
                $res[$key] = $item;
                continue;
            }

            if ($key == 'parking' && $item === true) {
                $res[$key] = $item;
                continue;
            }
        }
        if (empty($res)) {
            return false;
        }
        return $res;
    }


    public static function setFilter(&$query, $count = false, $exclude = [])
    {
        $json = file_get_contents(\yii\helpers\Url::to('@app/filter.json'));
        $filter = json_decode($json);


        $query->andWhere(['deleted' => 0]);
//        $query->andWhere(['is not','company_id',null]); //as  Sphinx
//        $query->andWhere(['>','verd',0]);//as  Sphinx


        $brand_model = null;
        if (CarsFilter::getData('brand_model')) {
            $brand_model = [];
            foreach (CarsFilter::getData('brand_model') as $bm) {
                $brand_model[] = preg_replace("/[^0-9]/", "", $bm);
            }

        }

        if (!in_array('brand_model', $exclude) && $brand_model) {
            $brands = [];
            $brands = CarsFilter::getData('brands');
            $ids = Framleidendur::find()->where(['model_id' => $brand_model])->all();
            $ids = ArrayHelper::getColumn($ids, 'id');

            foreach ($ids as $id) {
                unset($brands[array_search($id, $brands)]);
            }


            $ids = Framleidendur::find()->where(['id' => $brands])->all();
            $ids = ArrayHelper::getColumn($ids, 'model_id');


            $where_ids = [];
            foreach (array_merge($brand_model, $ids) as $ii) {
                if ($ii != 0) {
                    $where_ids[] = $ii;
                }
            }

            $query->andWhere(['in', 'ext_modgerdid', $where_ids]);
        } elseif (CarsFilter::getData('brands')) {
            $query->andWhere(['in', 'ext_modframleidandiid', CarsFilter::getData('brands')]);
        }


        if (!$count) {
            if (CarsFilter::getData('fuel_type')) {
                $query->andWhere(['in', 'eldsneyti', CarsFilter::getData('fuel_type')]);
//                $query->andWhere(['in', 'eldsneyti2', CarsFilter::getData('fuel_type')]);
            }

            if (!in_array('girartegund', $exclude) && CarsFilter::getData('girartegund')) {
                $query->andWhere(['girartegund' => CarsFilter::getData('girartegund')]);
            }

        }

        if (!in_array('argerd', $exclude) && CarsFilter::getData('argerd')) {
            $a = CarsFilter::getData('argerd');
            if (!isset($a[1]) || (isset($a[1]) && $filter->argerd->ma == $a[1])) {
                $a[1] = $filter->argerd->ma;
            }
            $query->andWhere(['between', 'argerd', $a[0], $a[1]]);
        }


        if (!in_array('verd', $exclude) && CarsFilter::getData('verd')) {
            $a = CarsFilter::getData('verd');
            if (!isset($a[1]) || (isset($a[1]) && $filter->verd->ma == $a[1])) {
                $a[1] = $filter->verd->ma;
            }
            $query->andWhere(['between', 'verd', (int)($a[0] / 1000), (int)($a[1] / 1000)]);
        }

        if (!in_array('ekinn', $exclude) && CarsFilter::getData('ekinn')) {
            $a = CarsFilter::getData('ekinn');
            if (!isset($a[1]) || (isset($a[1]) && $filter->ekinn->ma == $a[1])) {
                $a[1] = $filter->ekinn->ma;
            }

            $query->andWhere(['between', 'ekinn', (int)($a[0] / 1000), (int)($a[1] / 1000)]);
        }

        if (!in_array('flokkar', $exclude) && CarsFilter::getData('flokkar')) {
            $query->andWhere(['flokkur' => CarsFilter::getData('flokkar')]);
        }

        if (CarsFilter::getData('verdtilbod')) {
            $query->andWhere(['verdtilbod' => CarsFilter::getData('verdtilbod')]);
        }

        if (CarsFilter::getData('parking')) {
            $query->andWhere(['>', 'spot', 0]);
        }

        if (CarsFilter::getData('vt')) {
            $vt_sub = VehicleTypeSub::find()->where(['vehicle_type_id' => CarsFilter::getData('vt')])->all();
            $query->andWhere(['flokkur' => ArrayHelper::getColumn($vt_sub, 'flokkar_id')]);
        }


        if (!in_array('other', $exclude) && CarsFilter::getData('other')) {
            foreach (CarsFilter::getData('other') as $item) {
                $query->andWhere(['like', 'aukahlutir', "$" . $item . "$"]);
            }
        }

        $data = self::getData('all');

        if (isset($data['sort']['field']) && $data['sort']['field']) {
            $query->orderBy([$data['sort']['field'] => isset($data['sort']['order']) ? $data['sort']['order'] : 4]);
        }
    }

    public static function post($key = null)
    {
        $res = json_decode(file_get_contents('php://input'), true);
        if ($key) {
            if (isset($res[$key])) {
                return $res[$key];
            } else {
                return null;
            }
        } else {
            return $res;
        }
    }


    public static function getData($key = null)
    {
        if (self::$data) {
            $res = self::$data;
        } else {
            $res = self::post();
        }
        if ($key == 'all') {
            return $res;
        }

        $res = isset($res['filters']) ? $res['filters'] : $res;

        if ($key) {
            if (isset($res[$key])) {
                return $res[$key];
            } else {
                return [];
            }
        }
        return $res;
    }

    public static function getCarCars($car_is)
    {

        $car = Cars::findOne($car_is);

        $q = CarsSphinx::find();
        $q->select(self::$select);
        $q->limit(10);
        $q->andWhere(['deleted' => 0]);
        if ($car && $car->ext_modframleidandiid) {
            $q->andWhere(['ext_modframleidandiid' => $car->ext_modframleidandiid]);
        }
        return $q->asArray()->all();
//        return self::setDiscount($q->asArray()->all());
    }

    public static function setFilterNames($params)
    {
        $res = [];
        foreach ($params as $key => $param) {
            if ($key == 'sort') {

            } else if ($key == 'brands') {
                $brands = ArrayHelper::getColumn(Framleidendur::find()->where(['id' => $param])->groupBy('id')->all(), 'company');
                $res[$key] = "<a href='#'>" . join(', ', $brands) . "</a>";
            } elseif ($key == 'brand_model') {
                $res[$key] = "<a href='#'>" . join(', ', $brands) . "</a>";
                $brands = ArrayHelper::getColumn(Framleidendur::find()->where(['model_id' => $param])->all(), 'models');
            } elseif ($key == 'other') {
                $other = ArrayHelper::getColumn(Aukahlutir::find()->where(['id' => $param])->all(), 'name');
                $res[$key] = "<a href='#'>" . join(', ', $other) . "</a>";
            } elseif ($key == 'fuel_type') {
                $fuel_type = ArrayHelper::getColumn(Eldsneyti::find()->where(['id' => $param])->all(), 'name');
                $res[$key] = "<a href='#'>" . join(', ', $fuel_type) . "</a>";
            } elseif ($key == 'girartegund') {
                $girartegund = ArrayHelper::getColumn(Girartegund::find()->where(['id' => $param])->all(), 'name');
                $res[$key] = "<a href='#'>" . join(', ', $girartegund) . "</a>";
            } elseif ($key == 'verdtilbod') {
                $res[$key] = 'verdtilbod';
            } else {
                if ($key == 'argerd') {
                    $res[$key] = 'frá <a href="#">' . $param[0] . '</a> til <a href="#">' . $param[1] . "</a>";
                } else {
//                    if(!isset($param[1])){
//                        var_dump($param[1]);
//                    }
                    $res[$key] = 'frá <a href="#">' . self::numberFormat($param[0]) . '</a> til <a href="#">' . self::numberFormat($param[1]) . "</a>";
                }
            }
        }
        return $res;
    }


    public static function dayDiff($start)
    {
        $dStart = new \DateTime($start);
        $dEnd = new \DateTime();
        $dDiff = $dStart->diff($dEnd);
        return $dDiff->days;
    }

    public static function numberFormat($num)
    {
        return number_format($num, null, null, '.');
    }

    public static function generateRandomString($length = 255)
    {
        $res = Yii::$app->security->generateRandomString(255);

        $str = '';

        for ($i = 0; $i <= strlen($res) - 1; $i++) {
            if ($res[$i] != '-' && $res[$i] != '_') {
                $str .= $res[$i];
            }

            if ($i % 15 == 0) {
                $str .= "-";
            }
        }

        return mb_strimwidth($str,0,$length);
    }
}

?>