<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisement_placement_name".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 *
 * @property Advertisement[] $advertisements
 * @property AdvertisementPlacement[] $advertisementPlacements
 */
class AdvertisementPlacementName extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement_placement_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['title', 'description'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'title' => Yii::t('user', 'Title'),
            'description' => Yii::t('user', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['placement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisementPlacements()
    {
        return $this->hasMany(AdvertisementPlacement::className(), ['advertisement_placement_id' => 'id']);
    }
}
