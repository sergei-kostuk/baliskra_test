<?php

namespace app\models\sphinx;

use Yii;

/**
 * This is the model class for index "cars".
 *
 * @property integer $id
 * @property string $company_id
 * @property string $created
 * @property string $updated
 * @property string $car_id
 * @property integer $api_id
 * @property integer $eigandi
 * @property integer $flokkur
 * @property integer $argerd
 * @property integer $dyr
 * @property integer $manna
 * @property integer $ekinn
 * @property integer $litur
 * @property integer $drif
 * @property integer $girar
 * @property integer $eldsneyti
 * @property integer $sumardekk
 * @property integer $vetrardekk
 * @property integer $heilsarsdekk
 * @property integer $lowprofiledekk
 * @property integer $slagrymi
 * @property string $aukahlutirath
 * @property integer $skiptiodyrari
 * @property integer $skiptiodyrarikr
 * @property integer $skiptidyrari
 * @property integer $skiptidyrarikr
 * @property string $skiptiath
 * @property integer $naestaskodun
 * @property string $skrdags
 * @property integer $verd
 * @property integer $bilalan
 * @property integer $bilalanupphaed
 * @property integer $lanveitandiid
 * @property integer $bilalanafborgun
 * @property integer $dekkjastaerd
 * @property integer $hestofl
 * @property integer $afkostmaxkw
 * @property integer $strokkar
 * @property integer $tyngd
 * @property string $fyrstiskrdagur
 * @property integer $spot
 * @property string $verdath
 * @property integer $verdtilbod
 * @property integer $skrmanudur
 * @property integer $modelar
 * @property string $lastupdated
 * @property string $aukahlutir
 * @property integer $akeiningar
 * @property integer $bilalanlokagreidsla
 * @property integer $bilalanlanstimi
 * @property integer $burdargeta
 * @property integer $tjonabifreid
 * @property string $stadsetning
 * @property integer $nytt
 * @property integer $innflteg
 * @property integer $felgustaerd
 * @property integer $spennarafkerfis
 * @property integer $eftirafdekkjum
 * @property integer $oxlafjoldi
 * @property integer $nytimareim
 * @property integer $timareimkm
 * @property integer $anvsk
 * @property integer $stadsettpnr
 * @property integer $flokkur2
 * @property integer $flokkur3
 * @property integer $eldsneyti2
 * @property string $eydslainnanb
 * @property string $eydslautanb
 * @property string $eydslablandad
 * @property integer $co2
 * @property integer $skrath
 * @property integer $fjlykla
 * @property integer $fjlyklaremote
 * @property integer $timabunadur
 * @property integer $girartegund
 * @property integer $solulysing
 * @property integer $thskodun
 * @property integer $thskodunkm
 * @property integer $thskodundags
 * @property string $batterykw
 * @property integer $batterykm
 * @property integer $verdadurkr
 * @property integer $ext_imagecount
 * @property string $ext_modframleidandi
 * @property integer $ext_modframleidandiid
 * @property string $ext_modgerd
 * @property integer $ext_modgerdid
 * @property integer $ext_ekinnkm
 * @property integer $ext_argerd
 * @property integer $ext_statid
 * @property integer $ext_sortkey
 * @property integer $deleted
 * @property integer $deleted_at
 * @property array $photo_id
 * @property array $cid
 */
class CarsSphinx extends \yii\sphinx\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function indexName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id', 'api_id', 'eigandi', 'flokkur', 'argerd', 'dyr', 'manna', 'ekinn', 'litur', 'drif', 'girar', 'eldsneyti', 'sumardekk', 'vetrardekk', 'heilsarsdekk', 'lowprofiledekk', 'slagrymi', 'skiptiodyrari', 'skiptiodyrarikr', 'skiptidyrari', 'skiptidyrarikr', 'naestaskodun', 'verd', 'bilalan', 'bilalanupphaed', 'lanveitandiid', 'bilalanafborgun', 'dekkjastaerd', 'hestofl', 'afkostmaxkw', 'strokkar', 'tyngd', 'spot', 'verdtilbod', 'skrmanudur', 'modelar', 'akeiningar', 'bilalanlokagreidsla', 'bilalanlanstimi', 'burdargeta', 'tjonabifreid', 'nytt', 'innflteg', 'felgustaerd', 'spennarafkerfis', 'eftirafdekkjum', 'oxlafjoldi', 'nytimareim', 'timareimkm', 'anvsk', 'stadsettpnr', 'flokkur2', 'flokkur3', 'eldsneyti2', 'co2', 'skrath', 'fjlykla', 'fjlyklaremote', 'timabunadur', 'girartegund', 'solulysing', 'thskodun', 'thskodunkm', 'thskodundags', 'batterykm', 'verdadurkr', 'ext_imagecount', 'ext_modframleidandiid', 'ext_modgerdid', 'ext_ekinnkm', 'ext_argerd', 'ext_statid', 'ext_sortkey', 'deleted', 'deleted_at'], 'integer'],
            [['company_id', 'created', 'updated', 'car_id', 'aukahlutirath', 'skiptiath', 'skrdags', 'fyrstiskrdagur', 'verdath', 'lastupdated', 'aukahlutir', 'stadsetning', 'eydslainnanb', 'eydslautanb', 'eydslablandad', 'batterykw', 'ext_modframleidandi', 'ext_modgerd'], 'string'],
            [['photo_id', 'cid'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'car_id' => 'Car ID',
            'api_id' => 'Api ID',
            'eigandi' => 'Eigandi',
            'flokkur' => 'Flokkur',
            'argerd' => 'Argerd',
            'dyr' => 'Dyr',
            'manna' => 'Manna',
            'ekinn' => 'Ekinn',
            'litur' => 'Litur',
            'drif' => 'Drif',
            'girar' => 'Girar',
            'eldsneyti' => 'Eldsneyti',
            'sumardekk' => 'Sumardekk',
            'vetrardekk' => 'Vetrardekk',
            'heilsarsdekk' => 'Heilsarsdekk',
            'lowprofiledekk' => 'Lowprofiledekk',
            'slagrymi' => 'Slagrymi',
            'aukahlutirath' => 'Aukahlutirath',
            'skiptiodyrari' => 'Skiptiodyrari',
            'skiptiodyrarikr' => 'Skiptiodyrarikr',
            'skiptidyrari' => 'Skiptidyrari',
            'skiptidyrarikr' => 'Skiptidyrarikr',
            'skiptiath' => 'Skiptiath',
            'naestaskodun' => 'Naestaskodun',
            'skrdags' => 'Skrdags',
            'verd' => 'Verd',
            'bilalan' => 'Bilalan',
            'bilalanupphaed' => 'Bilalanupphaed',
            'lanveitandiid' => 'Lanveitandiid',
            'bilalanafborgun' => 'Bilalanafborgun',
            'dekkjastaerd' => 'Dekkjastaerd',
            'hestofl' => 'Hestofl',
            'afkostmaxkw' => 'Afkostmaxkw',
            'strokkar' => 'Strokkar',
            'tyngd' => 'Tyngd',
            'fyrstiskrdagur' => 'Fyrstiskrdagur',
            'spot' => 'Spot',
            'verdath' => 'Verdath',
            'verdtilbod' => 'Verdtilbod',
            'skrmanudur' => 'Skrmanudur',
            'modelar' => 'Modelar',
            'lastupdated' => 'Lastupdated',
            'aukahlutir' => 'Aukahlutir',
            'akeiningar' => 'Akeiningar',
            'bilalanlokagreidsla' => 'Bilalanlokagreidsla',
            'bilalanlanstimi' => 'Bilalanlanstimi',
            'burdargeta' => 'Burdargeta',
            'tjonabifreid' => 'Tjonabifreid',
            'stadsetning' => 'Stadsetning',
            'nytt' => 'Nytt',
            'innflteg' => 'Innflteg',
            'felgustaerd' => 'Felgustaerd',
            'spennarafkerfis' => 'Spennarafkerfis',
            'eftirafdekkjum' => 'Eftirafdekkjum',
            'oxlafjoldi' => 'Oxlafjoldi',
            'nytimareim' => 'Nytimareim',
            'timareimkm' => 'Timareimkm',
            'anvsk' => 'Anvsk',
            'stadsettpnr' => 'Stadsettpnr',
            'flokkur2' => 'Flokkur2',
            'flokkur3' => 'Flokkur3',
            'eldsneyti2' => 'Eldsneyti2',
            'eydslainnanb' => 'Eydslainnanb',
            'eydslautanb' => 'Eydslautanb',
            'eydslablandad' => 'Eydslablandad',
            'co2' => 'Co2',
            'skrath' => 'Skrath',
            'fjlykla' => 'Fjlykla',
            'fjlyklaremote' => 'Fjlyklaremote',
            'timabunadur' => 'Timabunadur',
            'girartegund' => 'Girartegund',
            'solulysing' => 'Solulysing',
            'thskodun' => 'Thskodun',
            'thskodunkm' => 'Thskodunkm',
            'thskodundags' => 'Thskodundags',
            'batterykw' => 'Batterykw',
            'batterykm' => 'Batterykm',
            'verdadurkr' => 'Verdadurkr',
            'ext_imagecount' => 'Ext Imagecount',
            'ext_modframleidandi' => 'Ext Modframleidandi',
            'ext_modframleidandiid' => 'Ext Modframleidandiid',
            'ext_modgerd' => 'Ext Modgerd',
            'ext_modgerdid' => 'Ext Modgerdid',
            'ext_ekinnkm' => 'Ext Ekinnkm',
            'ext_argerd' => 'Ext Argerd',
            'ext_statid' => 'Ext Statid',
            'ext_sortkey' => 'Ext Sortkey',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'photo_id' => 'Photo ID',
            'cid' => 'Cid',
        ];
    }
}
