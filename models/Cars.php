<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int $api_id
 * @property string $lu
 * @property int $old_price
 * @property int $company_id
 * @property int $cid
 * @property int $eigandi
 * @property int $flokkur
 * @property int $argerd
 * @property int $dyr
 * @property int $manna
 * @property int $ekinn
 * @property int $litur
 * @property int $drif
 * @property int $girar
 * @property int $eldsneyti
 * @property int $sumardekk
 * @property int $vetrardekk
 * @property int $heilsarsdekk
 * @property int $lowprofiledekk
 * @property int $slagrymi
 * @property string $aukahlutirath
 * @property int $skiptiodyrari
 * @property int $skiptiodyrarikr
 * @property int $skiptidyrari
 * @property int $skiptidyrarikr
 * @property string $skiptiath
 * @property int $naestaskodun
 * @property string $skrdags
 * @property int $verd
 * @property int $bilalan
 * @property int $bilalanupphaed
 * @property int $lanveitandiid
 * @property int $bilalanafborgun
 * @property int $dekkjastaerd
 * @property int $hestofl
 * @property int $afkostmaxkw
 * @property int $strokkar
 * @property int $tyngd
 * @property string $fyrstiskrdagur
 * @property int $spot
 * @property string $verdath
 * @property int $verdtilbod
 * @property int $skrmanudur
 * @property int $modelar
 * @property string $lastupdated
 * @property string $aukahlutir
 * @property int $akeiningar
 * @property int $bilalanlokagreidsla
 * @property int $bilalanlanstimi
 * @property int $burdargeta
 * @property int $tjonabifreid
 * @property string $stadsetning
 * @property int $nytt
 * @property int $innflteg
 * @property int $felgustaerd
 * @property int $spennarafkerfis
 * @property int $eftirafdekkjum
 * @property int $oxlafjoldi
 * @property int $nytimareim
 * @property int $timareimkm
 * @property int $anvsk
 * @property int $stadsettpnr
 * @property int $flokkur2
 * @property int $flokkur3
 * @property int $eldsneyti2
 * @property double $eydslainnanb
 * @property double $eydslautanb
 * @property double $eydslablandad
 * @property int $co2
 * @property int $skrath
 * @property int $fjlykla
 * @property int $fjlyklaremote
 * @property int $timabunadur
 * @property int $girartegund
 * @property int $solulysing
 * @property int $thskodun
 * @property int $thskodunkm
 * @property int $thskodundags
 * @property double $batterykw
 * @property int $batterykm
 * @property int $verdadurkr
 * @property int $ext_imagecount
 * @property string $ext_modframleidandi
 * @property int $ext_modframleidandiid
 * @property string $ext_modgerd
 * @property int $ext_modgerdid
 * @property int $ext_ekinnkm
 * @property int $ext_argerd
 * @property int $ext_statid
 * @property int $ext_sortkey
 * @property int $deleted
 * @property int $deleted_at
 * @property int $main_img
 * @property string $created
 * @property string $updated
 *
 * @property Companies $company
 * @property Eldsneyti $eldsneyti0
 * @property Eldsneyti $eldsneyti20
 * @property Flokkar $flokkur0
 * @property Framleidendur $extModframleidandi
 * @property Girartegund $girartegund0
 * @property Litir $litur0
 * @property CarsContactForm[] $carsContactForms
 * @property CarsFavorite[] $carsFavorites
 * @property CarsPhoto[] $carsPhotos
 * @property CarsProps[] $carsProps
 * @property CarsView[] $carsViews
 */
class Cars extends \app\models\BaseModel
{
    public $title;
    public $oldPrice;

    public $model_name;
    public $discount_price;
    public $on_website;
    public $page_views;
    public $in_search;
    public $contact_clicks;
    public $alerts;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['api_id'], 'required'],
            [['api_id', 'old_price', 'company_id', 'cid', 'eigandi', 'flokkur', 'argerd', 'dyr', 'manna', 'ekinn', 'litur', 'drif', 'girar', 'eldsneyti', 'sumardekk', 'vetrardekk', 'heilsarsdekk', 'lowprofiledekk', 'slagrymi', 'skiptiodyrari', 'skiptiodyrarikr', 'skiptidyrari', 'skiptidyrarikr', 'naestaskodun', 'verd', 'bilalan', 'bilalanupphaed', 'lanveitandiid', 'bilalanafborgun', 'dekkjastaerd', 'hestofl', 'afkostmaxkw', 'strokkar', 'tyngd', 'spot', 'verdtilbod', 'skrmanudur', 'modelar', 'akeiningar', 'bilalanlokagreidsla', 'bilalanlanstimi', 'burdargeta', 'tjonabifreid', 'nytt', 'innflteg', 'felgustaerd', 'spennarafkerfis', 'eftirafdekkjum', 'oxlafjoldi', 'nytimareim', 'timareimkm', 'anvsk', 'stadsettpnr', 'flokkur2', 'flokkur3', 'eldsneyti2', 'co2', 'skrath', 'fjlykla', 'fjlyklaremote', 'timabunadur', 'girartegund', 'solulysing', 'thskodun', 'thskodunkm', 'thskodundags', 'batterykm', 'verdadurkr', 'ext_imagecount', 'ext_modframleidandiid', 'ext_modgerdid', 'ext_ekinnkm', 'ext_argerd', 'ext_statid', 'ext_sortkey', 'deleted', 'deleted_at', 'main_img'], 'integer'],
            [['lu', 'lastupdated', 'created', 'updated'], 'safe'],
            [['aukahlutirath', 'verdath'], 'string'],
            [['eydslainnanb', 'eydslautanb', 'eydslablandad', 'batterykw'], 'number'],
            [['skiptiath', 'skrdags', 'fyrstiskrdagur', 'stadsetning', 'ext_modframleidandi', 'ext_modgerd'], 'string', 'max' => 50],
            [['aukahlutir'], 'string', 'max' => 600],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['eldsneyti'], 'exist', 'skipOnError' => true, 'targetClass' => Eldsneyti::className(), 'targetAttribute' => ['eldsneyti' => 'id']],
            [['eldsneyti2'], 'exist', 'skipOnError' => true, 'targetClass' => Eldsneyti::className(), 'targetAttribute' => ['eldsneyti2' => 'id']],
            [['flokkur'], 'exist', 'skipOnError' => true, 'targetClass' => Flokkar::className(), 'targetAttribute' => ['flokkur' => 'id']],
            [['ext_modframleidandiid'], 'exist', 'skipOnError' => true, 'targetClass' => Framleidendur::className(), 'targetAttribute' => ['ext_modframleidandiid' => 'id']],
            [['girartegund'], 'exist', 'skipOnError' => true, 'targetClass' => Girartegund::className(), 'targetAttribute' => ['girartegund' => 'id']],
            [['litur'], 'exist', 'skipOnError' => true, 'targetClass' => Litir::className(), 'targetAttribute' => ['litur' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'api_id' => Yii::t('app', 'Api ID'),
            'lu' => Yii::t('app', 'Lu'),
            'old_price' => Yii::t('app', 'Old Price'),
            'company_id' => Yii::t('app', 'Company ID'),
            'cid' => Yii::t('app', 'Cid'),
            'eigandi' => Yii::t('app', 'Eigandi'),
            'flokkur' => Yii::t('app', 'Flokkur'),
            'argerd' => Yii::t('app', 'Argerd'),
            'dyr' => Yii::t('app', 'Dyr'),
            'manna' => Yii::t('app', 'Manna'),
            'ekinn' => Yii::t('app', 'Ekinn'),
            'litur' => Yii::t('app', 'Litur'),
            'drif' => Yii::t('app', 'Drif'),
            'girar' => Yii::t('app', 'Girar'),
            'eldsneyti' => Yii::t('app', 'Eldsneyti'),
            'sumardekk' => Yii::t('app', 'Sumardekk'),
            'vetrardekk' => Yii::t('app', 'Vetrardekk'),
            'heilsarsdekk' => Yii::t('app', 'Heilsarsdekk'),
            'lowprofiledekk' => Yii::t('app', 'Lowprofiledekk'),
            'slagrymi' => Yii::t('app', 'Slagrymi'),
            'aukahlutirath' => Yii::t('app', 'Aukahlutirath'),
            'skiptiodyrari' => Yii::t('app', 'Skiptiodyrari'),
            'skiptiodyrarikr' => Yii::t('app', 'Skiptiodyrarikr'),
            'skiptidyrari' => Yii::t('app', 'Skiptidyrari'),
            'skiptidyrarikr' => Yii::t('app', 'Skiptidyrarikr'),
            'skiptiath' => Yii::t('app', 'Skiptiath'),
            'naestaskodun' => Yii::t('app', 'Naestaskodun'),
            'skrdags' => Yii::t('app', 'Skrdags'),
            'verd' => Yii::t('app', 'Verd'),
            'bilalan' => Yii::t('app', 'Bilalan'),
            'bilalanupphaed' => Yii::t('app', 'Bilalanupphaed'),
            'lanveitandiid' => Yii::t('app', 'Lanveitandiid'),
            'bilalanafborgun' => Yii::t('app', 'Bilalanafborgun'),
            'dekkjastaerd' => Yii::t('app', 'Dekkjastaerd'),
            'hestofl' => Yii::t('app', 'Hestofl'),
            'afkostmaxkw' => Yii::t('app', 'Afkostmaxkw'),
            'strokkar' => Yii::t('app', 'Strokkar'),
            'tyngd' => Yii::t('app', 'Tyngd'),
            'fyrstiskrdagur' => Yii::t('app', 'Fyrstiskrdagur'),
            'spot' => Yii::t('app', 'Spot'),
            'verdath' => Yii::t('app', 'Verdath'),
            'verdtilbod' => Yii::t('app', 'Verdtilbod'),
            'skrmanudur' => Yii::t('app', 'Skrmanudur'),
            'modelar' => Yii::t('app', 'Modelar'),
            'lastupdated' => Yii::t('app', 'Lastupdated'),
            'aukahlutir' => Yii::t('app', 'Aukahlutir'),
            'akeiningar' => Yii::t('app', 'Akeiningar'),
            'bilalanlokagreidsla' => Yii::t('app', 'Bilalanlokagreidsla'),
            'bilalanlanstimi' => Yii::t('app', 'Bilalanlanstimi'),
            'burdargeta' => Yii::t('app', 'Burdargeta'),
            'tjonabifreid' => Yii::t('app', 'Tjonabifreid'),
            'stadsetning' => Yii::t('app', 'Stadsetning'),
            'nytt' => Yii::t('app', 'Nytt'),
            'innflteg' => Yii::t('app', 'Innflteg'),
            'felgustaerd' => Yii::t('app', 'Felgustaerd'),
            'spennarafkerfis' => Yii::t('app', 'Spennarafkerfis'),
            'eftirafdekkjum' => Yii::t('app', 'Eftirafdekkjum'),
            'oxlafjoldi' => Yii::t('app', 'Oxlafjoldi'),
            'nytimareim' => Yii::t('app', 'Nytimareim'),
            'timareimkm' => Yii::t('app', 'Timareimkm'),
            'anvsk' => Yii::t('app', 'Anvsk'),
            'stadsettpnr' => Yii::t('app', 'Stadsettpnr'),
            'flokkur2' => Yii::t('app', 'Flokkur2'),
            'flokkur3' => Yii::t('app', 'Flokkur3'),
            'eldsneyti2' => Yii::t('app', 'Eldsneyti2'),
            'eydslainnanb' => Yii::t('app', 'Eydslainnanb'),
            'eydslautanb' => Yii::t('app', 'Eydslautanb'),
            'eydslablandad' => Yii::t('app', 'Eydslablandad'),
            'co2' => Yii::t('app', 'Co2'),
            'skrath' => Yii::t('app', 'Skrath'),
            'fjlykla' => Yii::t('app', 'Fjlykla'),
            'fjlyklaremote' => Yii::t('app', 'Fjlyklaremote'),
            'timabunadur' => Yii::t('app', 'Timabunadur'),
            'girartegund' => Yii::t('app', 'Girartegund'),
            'solulysing' => Yii::t('app', 'Solulysing'),
            'thskodun' => Yii::t('app', 'Thskodun'),
            'thskodunkm' => Yii::t('app', 'Thskodunkm'),
            'thskodundags' => Yii::t('app', 'Thskodundags'),
            'batterykw' => Yii::t('app', 'Batterykw'),
            'batterykm' => Yii::t('app', 'Batterykm'),
            'verdadurkr' => Yii::t('app', 'Verdadurkr'),
            'ext_imagecount' => Yii::t('app', 'Ext Imagecount'),
            'ext_modframleidandi' => Yii::t('app', 'Ext Modframleidandi'),
            'ext_modframleidandiid' => Yii::t('app', 'Ext Modframleidandiid'),
            'ext_modgerd' => Yii::t('app', 'Ext Modgerd'),
            'ext_modgerdid' => Yii::t('app', 'Ext Modgerdid'),
            'ext_ekinnkm' => Yii::t('app', 'Ext Ekinnkm'),
            'ext_argerd' => Yii::t('app', 'Ext Argerd'),
            'ext_statid' => Yii::t('app', 'Ext Statid'),
            'ext_sortkey' => Yii::t('app', 'Ext Sortkey'),
            'deleted' => Yii::t('app', 'Deleted'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'main_img' => Yii::t('app', 'Main Img'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts()
    {
        return $this->hasMany(Alerts::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEldsneyti0()
    {
        return $this->hasOne(Eldsneyti::className(), ['id' => 'eldsneyti']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEldsneyti20()
    {
        return $this->hasOne(Eldsneyti::className(), ['id' => 'eldsneyti2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlokkur0()
    {
        return $this->hasOne(Flokkar::className(), ['id' => 'flokkur']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtModframleidandi()
    {
        return $this->hasOne(Framleidendur::className(), ['id' => 'ext_modframleidandiid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirartegund0()
    {
        return $this->hasOne(Girartegund::className(), ['id' => 'girartegund']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLitur0()
    {
        return $this->hasOne(Litir::className(), ['id' => 'litur']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsContactForms()
    {
        return $this->hasMany(CarsContactForm::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsFavorites()
    {
        return $this->hasMany(CarsFavorite::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsPhotos()
    {
        return $this->hasMany(CarsPhoto::className(), ['car_id' => 'id'])->where(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsProps()
    {
        return $this->hasOne(CarsProps::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarsViews()
    {
        return $this->hasMany(CarsView::className(), ['car_id' => 'id']);
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub
        $this->oldPrice = $this->getOldPrice();
        $this->title = Html::tag('strong', $this->ext_modframleidandi) . " " . $this->ext_modgerd;
        if(isset($this->extModframleidandi)){
            $this->model_name = $this->extModframleidandi->models;
        }

        $this->discount_price = ($this->discount_price ?? 0) . " kr";

        $this->on_website = CarsFilter::dayDiff($this->created) . ' days';
        $this->in_search = isset($this->carsProps->in_search) ? $this->carsProps->in_search : 0;
        $this->contact_clicks = isset($this->carsProps->contact_clicks) ? $this->carsProps->contact_clicks : 0;
        $this->alerts = $this->alerts ?? 0;
    }


    public static function priceFormat($price)
    {
        return CarsFilter::numberFormat($price * 1000);
    }

    public function getTrands()
    {
        $cars_in_sale = Cars::find()->where(['deleted' => 0])
            ->andWhere(['>=', 'created', date('Y-m-d', strtotime('-30 days', time()))])->count();
        $active_alerts = Alerts::find()->where(['is', 'deleted', null])
            ->andWhere(['>=', 'created', date('Y-m-d', strtotime('-30 days', time()))])->count();
        $visits = CarsViewKey::find()->where(['>', 'created', date('Y-m-d', strtotime('- 1 week'))])->count();
        $contacts = CarsContactForm::find()->where(['>=', 'created', date('Y-m-d', strtotime('-30 days', time()))])->count();

        return (Object)[
            'cars_in_sale' => $cars_in_sale,
            'active_alerts' => $active_alerts,
            'visits' => $visits,
            'contacts' => $contacts
        ];
    }

    public function getOldPrice()
    {
        $oldPrice = History::find()->where(['item_id' => $this->id])->orderBy(['id' => SORT_DESC])->one();
        if ($oldPrice) {
            $oldPrice = $oldPrice->old_value;
            return $oldPrice;
        }
        return null;
    }
}
