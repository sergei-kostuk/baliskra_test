<?php
//Yii::setAlias('@webLang', '@web/' . \Yii::$app->language);
?>
<!-- BEGIN: Aside Menu -->
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1"
     m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <?php foreach ($items as $item): ?>
            <?php if (!isset($item['role']) || (isset($item['role']) && Yii::$app->user->can($item['role']))): ?>


                <li class="m-menu__item  m-menu__item--submenu <?= isset($item['url']) && strstr($item['url'], 'cur-url') ? ' m-menu__item--active' : ''; ?>"
                    aria-haspopup="true" m-menu-submenu-toggle="hover">
                    <a href="<?= isset($item['url']) ? \yii\helpers\Url::to($item['url']) : 'javascript:'; ?>"
                       class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon <?= $item['icon']; ?>"></i>
                        <span class="m-menu__link-text"><?= $item['label']; ?></span>
                        <?php if (isset($item['items']) && !empty($item['items'])): ?>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        <?php endif; ?>
                    </a>
                    <?php if (isset($item['items']) && !empty($item['items'])): ?>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text"><?= $item['label']; ?></span>
											</span>
                                </li>
                                <?php foreach ($item['items'] as $subItem): ?>
                                    <li class="m-menu__item " aria-haspopup="true">
                                        <a href="<?= isset($subItem['url']) ? \yii\helpers\Url::to($subItem['url']) : 'javascript:'; ?>"
                                           class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text"><?= $subItem['label']; ?></span>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>